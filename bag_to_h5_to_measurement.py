import h5py
import curie as ci
import trajan as tr
import mfdf
import os
import subprocess


def h5_to_latlon(h5_file):
    with h5py.File(h5_file, "r") as f:
        lat = f["mavros/global_position/global/latitude"][()]
        lon = f["mavros/global_position/global/longitude"][()]
        alt = f["mavros/global_position/global/altitude"][()]
        return lat, lon, alt


def h5_to_traj(h5_file):
    with h5py.File(h5_file, "r") as f:
        x = f["mavros/global_position/local/pose.pose.position.x"][()]
        y = f["mavros/global_position/local/pose.pose.position.y"][()]
        z = f["mavros/global_position/local/pose.pose.position.z"][()]
        qx = f["mavros/global_position/local/pose.pose.orientation.x"][()]
        qy = f["mavros/global_position/local/pose.pose.orientation.y"][()]
        qz = f["mavros/global_position/local/pose.pose.orientation.z"][()]
        qw = f["mavros/global_position/local/pose.pose.orientation.w"][()]
        times = f["mavros/global_position/local/header.stamp"][()]
        rad = ci.Listmode.read(f["/rad"])
    traj = tr.Trajectory(
        translations=tr.Translations(x, y, z),
        rotations=tr.Rotations.from_quaternions(qw, qx, qy, qz),
        ts=times,
        header=tr.Header(),
    )
    return traj, rad


def create_world(trajectory, recon_config):
    # Create dense ground plane
    meta = tr.VoxelGridMeta.from_trajectory(
        trajectory,
        delta=recon_config.get_param("voxel_size"),
        pad=recon_config.get_param(
            "path_padding"
        ),  # 20#job.dynamic_params["trajectory_pad"],
    )
    # Assume the trajectory starts on the ground
    world = tr.VoxelGrid(meta=meta, dense=True).cut(
        ranges={
            "z": (
                trajectory.pz[0] - 0.5 - 1e-6,
                trajectory.pz[0] - 0.5 + 1e-6,
            )
        }
    )
    return world


def h5_to_measurement(h5_file, recon_config, sys_config):
    traj, rad = h5_to_traj(h5_file)
    # filter out bad data
    traj = traj.cut(ranges={"px": [-1e5, 1e5], "py": [-1e5, 1e5], "pz": [-1e5, 1e5]})
    world = create_world(traj, recon_config)
    meas = mfdf.Measurement(rad, traj, world, sys_config)
    # fix header
    meas.traj.header.child_frame_id = "base_link"
    meas.traj.header.frame_id = "map"
    meas.transform_traj_to_det_frame()
    return meas


def subfolders_to_h5(root_folder_path, miniprism=False):
    """Convert all .bag or .bag.active files in all subfolders of a given folder to .h5 files."""
    h5_files = []
    for root, dirs, files in os.walk(root_folder_path):
        for dir in dirs:
            print("Converting folder {}".format(dir))
            dir_path = os.path.join(root, dir)
            h5_file = folder_to_h5(dir_path, miniprism)
            if h5_file is not None:
                h5_files.append(h5_file)
    return h5_files


def folder_to_h5(meas_folder_path, miniprism=False):
    """Convert a .bag or .bag.active file in a given folder to .h5 file."""
    all_h5_files = [f for f in os.listdir(meas_folder_path) if f.endswith(".h5")]
    assert len(all_h5_files) <= 1, "More than one .h5 file found in {}".format(
        meas_folder_path
    )
    all_bag_files = [f for f in os.listdir(meas_folder_path) if f.endswith(".bag")]
    all_bag_files = [f for f in all_bag_files if not f.endswith(".orig.bag")]
    assert len(all_bag_files) <= 1, "More than one .bag file found in {}".format(
        meas_folder_path
    )
    all_bag_active_files = [
        f for f in os.listdir(meas_folder_path) if f.endswith(".bag.active")
    ]
    assert (
        len(all_bag_active_files) <= 1
    ), "More than one .bag.active file found in {}".format(meas_folder_path)
    current_dir = os.getcwd()
    if miniprism:
        bag_to_h5_script = os.path.join(current_dir, "bag_to_h5_miniprism.sh")
    else:
        bag_to_h5_script = os.path.join(current_dir, "bag_to_h5.sh")
    if len(all_h5_files) == 1:
        h5_file = os.path.join(meas_folder_path, all_h5_files[0])
        print("Found {} file in {}".format(all_h5_files[0], meas_folder_path))
    elif len(all_bag_files) == 1:
        bag_file = os.path.join(meas_folder_path, all_bag_files[0])
        print("Found {} file in {}".format(all_bag_files[0], meas_folder_path))
        result = subprocess.run(
            [f"{bag_to_h5_script} {bag_file}"],
            check=False,
            capture_output=False,
            shell=True,
            cwd=meas_folder_path,
        )
        h5_file = os.path.join(meas_folder_path, all_bag_files[0] + ".h5")
    elif len(all_bag_active_files) == 1:
        bag_file_active = os.path.join(meas_folder_path, all_bag_active_files[0])
        print("Found {} file in {}".format(all_bag_active_files[0], meas_folder_path))
        result = subprocess.run(
            [f"{bag_to_h5_script} {bag_file_active}"],
            check=False,
            capture_output=False,
            shell=True,
            cwd=meas_folder_path,
        )
        h5_file = os.path.join(meas_folder_path, "data.bag.h5")
    else:
        print("No .bag, .bag.active, or .h5 file found in {}".format(meas_folder_path))
        return None

    return h5_file


def folder_to_measurement(meas_folder_path, recon_config=None, sys_config=None):
    """Loads a measurement from a given folder."""
    h5_file = folder_to_h5(meas_folder_path)
    if recon_config is not None and sys_config is not None:
        return h5_to_measurement(h5_file, recon_config, sys_config)
