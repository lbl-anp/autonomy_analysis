#!/bin/bash

# Check if the argument is provided
if [ -z "$1" ]; then
    echo "Usage: $0 /absolute/path/to/data.bag or bag.active"
    exit 1
fi

# Extract file path and name from the argument
file="$1"
file_dir="$(dirname "$file")"
file_name="$(basename "$file")"

# Activate the conda environment
source /home/emil/miniconda3/etc/profile.d/conda.sh
conda activate rot
# source activate rot <- doesn't work from python scripts
export ROT_PATH=/home/emil/repos/lamp_stack/config/systems

# Change to the directory containing the .bag file
cd "$file_dir" || exit

# Process the .bag file
echo "Processing $file"
if [[ "$file_name" == *.active ]]; then
    # Initialize rot
    rot init "$file_name"
    rot rosbag reindex "$file_name"
    rot rosbag fix --force "$file_name" data.bag
    file_name="data.bag"
fi

# Convert legacy format to new
rot legacy-imu data.bag
rm env.list
rot init --system=miniprism "$file_name"

# If env.list is empty, bag needs to be reindexed
if [[ ! -s env.list ]]; then
    rot rosbag reindex "$file_name"
    rm env.list
    rot init "$file_name"
fi

# Convert .bag file to h5 with additional messages
# rot h5 --additional-messages all "$file_name"
# Convert .bag file to h5 without images
rot h5 --overwrite --additional-messages /an_device/Imu,/count_vs_time,/imu_ros,/interaction_data,/mavros/global_position/compass_hdg,/mavros/global_position/global,/mavros/global_position/local,/mavros/global_position/raw/fix,/mavros/global_position/raw/gps_vel,/mavros/global_position/rel_alt,/sensor_diagnostics,/gps_ros,/nanomca,/ch_spec "$file_name"

# Go back to the original directory
cd - || exit
