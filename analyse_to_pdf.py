import pandas as pd
import h5py
import os
import sys

# import curie as ci
import numpy as np
import mfdf
import trajan as tr
import radkit_lamp as rkl
import bag_to_h5_to_measurement
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import textwrap
from reportlab.lib.pagesizes import letter, landscape
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import (
    SimpleDocTemplate,
    Table,
    TableStyle,
    Paragraph,
    Image,
    PageBreak,
)


def shiftline(traj, nparts=100):
    z = traj.pz
    x = traj.px
    y = traj.py
    df = pd.DataFrame({"x": x, "y": y, "z": z})
    colors = plt.cm.plasma(np.linspace(0, 1, nparts))
    fig = plt.figure(figsize=(5, 4), dpi=100)
    ax = fig.add_subplot(111, projection="3d")
    ax.view_init(elev=40, azim=-70)

    # Make the box aspect ratio 1:1:1
    ax.set_box_aspect([np.ptp(df.x), np.ptp(df.y), np.ptp(df.z)])

    # Generate colorbar
    norm = plt.Normalize(0, 1)
    sm = plt.cm.ScalarMappable(cmap=plt.cm.plasma, norm=norm)
    sm.set_array([])
    cbar = fig.colorbar(
        sm, label="Time", shrink=0.4, aspect=10, pad=0.25
    )  # Added 'pad' parameter

    n = round(len(df) / nparts)
    for i in range(nparts):
        ax.plot(
            df.iloc[i * n : (i + 1) * n + 1].x,
            df.iloc[i * n : (i + 1) * n + 1].y,
            df.iloc[i * n : (i + 1) * n + 1].z,
            color=colors[i],
        )

    ax.set_xlabel("X")
    ax.set_ylabel("Y")
    ax.set_zlabel("Z")
    fig.set_tight_layout(True)
    return fig, ax


# # Reading the CSV file
# base_path = "/home/emil/data/RFS/2023_08_29_RSF_Autonomy_and_grid_stops"
# base_path = "/home/emil/data/RFS/2023_07_13_overshoot_chase_and_x-dips"
# df = pd.read_csv("/home/emil/data/RFS/2023_08_29_RSF_Autonomy_and_grid_stops/2023_08_30_runs.csv")
# df = pd.read_csv("/home/emil/data/RFS/2023_07_13_overshoot_chase_and_x-dips/run_info.csv")
# filtered_df = df[df['analyse'] == 1]
# # filtered_df = df


def generate_pdf(input_folder, notes_df=None):
    # TODO read these from the env.list instead
    sys_config, recon_config = mfdf.io.load_configs(*rkl.get_configs("nglamp"))
    recon_config.set_param("E", 662.0)
    recon_config.set_param("path_padding", 20)
    recon_config.set_param("voxel_size", 0.5)

    folder_name = os.path.basename(input_folder)
    base_path = os.path.dirname(input_folder)

    # Initialize PDF
    pdf = SimpleDocTemplate(
        os.path.join(input_folder, f"{folder_name}.pdf"),
        pagesize=(1440, 1080),
        # pagesize=landscape((720, 540))
    )
    flowables = []
    styles = getSampleStyleSheet()

    # Summary table data and heading
    summary_data = [["Run Name", "Flight \nTime (s)", "Description"]]
    flowables.append(Paragraph("Summary Table", styles["Heading1"]))

    if notes_df is not None:
        # Loop through filtered DataFrame
        for index, row in notes_df.iterrows():
            bag_name = row["bag_name"]
            comment = row.get("comment", "no description")
            comment_wrap = textwrap.fill(
                repr(comment), width=30
            )  # Wrap text at 30 characters
            description = row.get("description", "no description")
            description_wrap = textwrap.fill(
                repr(description), width=30
            )  # Wrap text at 30 characters
            measurement_folder = os.path.join(base_path, bag_name)
            append_to_pdf(
                measurement_folder,
                bag_name,
                recon_config,
                sys_config,
                flowables,
                summary_data,
                description_wrap,
                comment_wrap,
            )
    else:
        subfolders = [f.path for f in os.scandir(input_folder) if f.is_dir()]
        for subfolder in subfolders:
            folder_name = os.path.basename(subfolder)
            append_to_pdf(
                subfolder,
                folder_name,
                recon_config,
                sys_config,
                flowables,
                summary_data,
                "",
                "",
            )

    # Add summary table to PDF
    summary_table = Table(summary_data, colWidths=[130, 70, 200])
    summary_table.setStyle(
        TableStyle(
            [
                ("BACKGROUND", (0, 0), (-1, 0), colors.grey),
                ("TEXTCOLOR", (0, 0), (-1, 0), colors.whitesmoke),
                ("ALIGN", (0, 0), (-1, -1), "CENTER"),
                ("FONTNAME", (0, 0), (-1, 0), "Helvetica-Bold"),
                ("FONTSIZE", (0, 0), (-1, 0), 14),
                ("BOTTOMPADDING", (0, 0), (-1, 0), 12),
                ("BACKGROUND", (0, 1), (-1, -1), colors.beige),
                ("GRID", (0, 0), (-1, -1), 1, colors.black),
                ("WORDWRAP", (2, 1), (2, -1), "CJK"),
            ]
        )
    )  # Wrapping text in column 2
    flowables.insert(
        1, summary_table
    )  # Insert the summary table after the "Summary Table" heading
    # Build PDF
    pdf.build(flowables)


def append_to_pdf(
    measurement_folder,
    bag_name,
    recon_config,
    sys_config,
    flowables,
    summary_data,
    description_wrap="",
    comment_wrap="",
):

    styles = getSampleStyleSheet()
    try:
        meas = bag_to_h5_to_measurement.folder_to_measurement(
            measurement_folder, recon_config, sys_config
        )
    except:
        print(f"Failed to make measurement of {bag_name}")
        # flowables.append(PageBreak())
        # flowables.append(Paragraph(f"{bag_name} failed to make a measurement object.", styles["Heading2"]))
        summary_data.append([bag_name, "N/A", "Failed to load", comment_wrap])
        return

    # Calculate flight time as time spent above 4m
    idx = np.where(meas.traj.pz > 4)[0]
    flight_time = round(meas.traj.ts[idx[-1]] - meas.traj.ts[idx[0]]) if idx.size else 0
    summary_data.append(
        [bag_name, round(meas.traj.duration), description_wrap, comment_wrap]
    )

    # Add individual Trajectory analysis to PDF
    flowables.append(PageBreak())
    flowables.append(Paragraph(f"Analysis for {bag_name}", styles["Heading1"]))
    # Perform reconstruction
    status = mfdf.tools.status.Status(name="mfdf recon", quiet=True)
    recon = mfdf.Reconstruction(measurement=meas, config=recon_config, status=status)
    image_gpsl, bkgs_gpsl = recon.compute_gpsl()

    # Plotting
    fig = plt.figure(figsize=(8, 4), dpi=150)
    image_gpsl.plot(
        prop="zscores",  # which property of the voxelgrid to plot
        plane="xy",  # which plane for projection
        reduction="min",  # how to reduce on the projection
        mask_outliers=False,  # whether to mask out values outside the vrange
        vmin=None,  # minimum value of the colorscale
        vmax=5,  # maximum value of the colorscale
        draw_colorbar=True,  # whether to draw the colobar
        colorbar_kwargs=None,  # additional kwargs sent to the colorbar creation call
        unmask_all=True,  # plot all voxels (unmasked) using fill values or just the occupied (masked)
        cmap="plasma_r",  # colormap
        cmap_discretize=5,  # whether to discretize the cmap and with how many discretizations
        norm=None,  # normalization
        pc_pts=5e5,  # additional kwargs sent to Measurement.plot()
        pc_kwargs={"colorize_by": "intensity", "vmax": 100, "cmap": "Greys_r"},
        # xmin=-20,
        # ymin=-40,
        # zmin=None,
        # xmax=60,
        # ymax=20,
        # zmax=None,
    )
    fig.savefig(os.path.join(measurement_folder, f"plots/{bag_name}.png"))

    fig, ax = shiftline(meas.traj)
    fig.savefig(os.path.join(measurement_folder, f"plots/{bag_name}_3D.png"))

    fig, ax = plt.subplots(figsize=(3, 2.5), dpi=100)
    ax.hist(meas.traj.pz, bins=100)
    ax.set_yscale("log")
    ax.set_xlabel("Height")
    ax.set_ylabel("Time spent")
    fig.set_tight_layout(True)
    fig.savefig(os.path.join(measurement_folder, f"plots/{bag_name}_height_hist.png"))

    fig, ax = plt.subplots(dpi=100, figsize=(3, 3))
    meas.rad.plot_waterfall(norm=LogNorm(), ax=ax)
    ax.set_xlim([0, 1000])
    fig.set_tight_layout(True)
    fig.savefig(os.path.join(measurement_folder, f"plots/{bag_name}_waterfall.png"))

    fig = plt.figure(figsize=(4, 3), dpi=150)
    meas.plot_counts(
        time_edges=meas.traj.ts_edges,  # how to bin time
        normalize_by_time=False,  # divide by time bin widths
        # energy_bounds=[600,700],            # energy ROI
        # sum_dets=False,                     # whether to sum up individual detectors
        sum_dets=True,  # whether to sum up individual detectors
    )
    # meas.plot_counts(
    #         time_edges=meas.traj.ts_edges,
    #         normalize_by_time=False,
    #         energy_bounds=[600,700],
    #         sum_dets=True
    # )
    fig.set_tight_layout(True)
    plt.legend()
    fig.savefig(os.path.join(measurement_folder, f"plots/{bag_name}_counts.png"))

    # plot height over time
    fig, ax = plt.subplots(dpi=150, figsize=(3, 2.5))
    ax.plot(meas.traj.ts - meas.traj.ts[0], meas.traj.pz)
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Height (m)")
    fig.set_tight_layout(True)
    fig.savefig(os.path.join(measurement_folder, f"plots/{bag_name}_height.png"))

    fig = plt.figure(figsize=(4, 3), dpi=150)
    meas.plot_spectrum(
        energy_edges=np.linspace(0, 1500, 300),  # how to bin in energy
        sum_dets=False,  # whether to sum up individual detectors
        normalize_by_energy=True,  # divide by energy bin widths
        normalize_by_time=True,  # divide by time to get rate
    )
    meas.plot_spectrum(
        energy_edges=np.linspace(0, 1500, 300),
        sum_dets=True,
        normalize_by_energy=True,
        normalize_by_time=True,
    )
    plt.legend()
    fig.set_tight_layout(True)
    fig.savefig(os.path.join(measurement_folder, f"plots/{bag_name}_spectrum.png"))
    plt.close(fig)

    # Create a list of Image flowables for the plots
    plot_images = [
        [
            Image(
                os.path.join(measurement_folder, f"plots/{bag_name}.png"),
                width=250,
                height=180,
            ),
            Image(
                os.path.join(measurement_folder, f"plots/{bag_name}_3D.png"),
                width=250,
                height=180,
            ),
            Image(
                os.path.join(measurement_folder, f"plots/{bag_name}_counts.png"),
                width=250,
                height=180,
            ),
        ],
        [
            Image(
                os.path.join(measurement_folder, f"plots/{bag_name}_height.png"),
                width=250,
                height=180,
            ),
            Image(
                os.path.join(measurement_folder, f"plots/{bag_name}_height_hist.png"),
                width=250,
                height=180,
            ),
            Image(
                os.path.join(measurement_folder, f"plots/{bag_name}_waterfall.png"),
                width=250,
                height=180,
            ),
            Image(
                os.path.join(measurement_folder, f"plots/{bag_name}_spectrum.png"),
                width=250,
                height=180,
            ),
        ],
    ]

    # Create a ReportLab Table to place the images next to each other
    # img_table = Table([plot_images], colWidths=260)
    img_table = Table(plot_images, colWidths=260, rowHeights=260)
    img_table.setStyle(
        TableStyle(
            [
                ("ALIGN", (0, 0), (-1, -1), "CENTER"),
                ("VALIGN", (0, 0), (-1, -1), "MIDDLE"),
            ]
        )
    )

    # Add plot table to PDF flowables
    flowables.append(
        Paragraph(
            f"Bag time: {round(meas.traj.duration)}s, air time: {flight_time}s, {description_wrap}",
            styles["Heading2"],
        )
    )
    flowables.append(img_table)
    return


if __name__ == "__main__":
    if len(sys.argv) == 2:
        generate_pdf(sys.argv[1])
    elif len(sys.argv) == 3:
        generate_pdf(sys.argv[1], sys.argv[2])
    else:
        print(
            "Give a folder containing folders of .h5 files and optionally a csv that describes the measurements."
        )
