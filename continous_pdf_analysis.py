import os
import time
import subprocess
import bag_to_h5_to_measurement
import pandas as pd
import h5py
import os
import curie as ci
import numpy as np
import mfdf
import trajan as tr
import radkit_lamp as rkl

import bag_to_h5_to_measurement
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import textwrap
from reportlab.lib.pagesizes import letter, landscape
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph, Image, PageBreak


def find_bag_files(root_folder):
    bag_files = []
    for root, dirs, files in os.walk(root_folder):
        for file in files:
            if file.endswith("data.bag"):
                bag_files.append(os.path.join(root, file))
    return bag_files

if __name__ == "__main__":
    processed_files = set()
    root_folder = "/home/lbnl/data/"

    while True:
        current_files = set(find_bag_files(root_folder))
        new_files = current_files - processed_files

        for bag_file in new_files:
            current_dir = os.getcwd()
            subprocess.run([f"./bag_to_h5.sh {bag_file}"], check=False, capture_output=False, shell=True, cwd=current_dir)
            processed_files.add(bag_file)

        time.sleep(1)
