import math


class LatLon2XY:
    """
    Class to convert latitude and longitude to Cartesian coordinates and vice versa
    based on a local reference point.
    """

    def __init__(self, ref_lat, ref_lon, ref_x, ref_y):
        """
        Initialize LatLon2XY object with a local reference point.

        Parameters:
            ref_lat (float): Latitude of the reference point in degrees.
            ref_lon (float): Longitude of the reference point in degrees.
            ref_x (float): X coordinate of the reference point.
            ref_y (float): Y coordinate of the reference point.
        """
        self.ref_lat = math.radians(ref_lat)
        self.ref_lon = math.radians(ref_lon)
        self.ref_x = ref_x
        self.ref_y = ref_y

        # WGS84 ellipsoid constants
        self.a = 6378137  # semi-major axis in meters
        self.b = 6356752.3142  # semi-minor axis in meters
        self.e = math.sqrt(1 - (self.b / self.a) ** 2)  # eccentricity

    def get_xy(self, lat, lon):
        """
        Convert latitude and longitude to Cartesian x and y coordinates.

        Parameters:
            lat (float): Latitude in degrees.
            lon (float): Longitude in degrees.

        Returns:
            tuple: Cartesian coordinates (x, y)
        """
        lat = math.radians(lat)
        lon = math.radians(lon)

        N = self.a / math.sqrt(1 - self.e**2 * math.sin(self.ref_lat) ** 2)
        x = (lon - self.ref_lon) * N * math.cos(self.ref_lat)
        y = (lat - self.ref_lat) * N

        return x + self.ref_x, y + self.ref_y

    def get_latlon(self, x, y):
        """
        Convert Cartesian x and y coordinates to latitude and longitude.

        Parameters:
            x (float): X coordinate.
            y (float): Y coordinate.

        Returns:
            tuple: Geographic coordinates (latitude, longitude) in degrees.
        """
        N = self.a / math.sqrt(1 - self.e**2 * math.sin(self.ref_lat) ** 2)
        lat = (y - self.ref_y) / N + self.ref_lat
        lon = (x - self.ref_x) / (N * math.cos(self.ref_lat)) + self.ref_lon

        return math.degrees(lat), math.degrees(lon)
