import cv2
import numpy as np
from PIL import Image
import io

def make_video(figs, fps=5, filename="reconstruction_timeline.mp4"):
    # Convert matplotlib figures to frames
    frames = []
    for fig in figs:
        # Save figure to a temporary buffer
        buf = io.BytesIO()
        fig.savefig(buf, format='png', bbox_inches='tight')
        buf.seek(0)
        
        # Convert to PIL Image
        pil_img = Image.open(buf)
        
        # Convert PIL image to numpy array for OpenCV
        frame = cv2.cvtColor(np.array(pil_img), cv2.COLOR_RGB2BGR)
        frames.append(frame)
        buf.close()

    # Get dimensions of the first frame
    height, width = frames[0].shape[:2]

    # Create video writer
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')  # or 'avc1' for h264 codec
    out = cv2.VideoWriter('reconstruction_timeline.mp4', fourcc, 5, (width, height))

    # Write frames to video
    for frame in frames:
        out.write(frame)
        
        # Optional: add pause at the end by duplicating last frame
        if frame is frames[-1]:
            for _ in range(20):  # Add 20 frames (4 seconds at 5 fps) of the last image
                out.write(frame)

    # Release the video writer
    out.release()
    print(f"Video saved to {filename}")

def make_gif(figs, filename="reconstruction_timeline.gif"):
    # Convert matplotlib figures to PIL Images
    images = []
    buffers = []  # Keep buffers in memory until GIF is saved
    for fig in figs:
        # Save figure to a temporary buffer
        buf = io.BytesIO()
        fig.savefig(buf, format='png', bbox_inches='tight')
        buf.seek(0)
        
        # Create PIL Image from buffer
        img = Image.open(buf)
        images.append(img)
        buffers.append(buf)  # Store buffer in list

    # Duplicate the last frame 4 more times (5 total)
    last_frame = images[-1]
    for _ in range(4):
        images.append(last_frame)

    # Save as GIF
    images[0].save(
        'reconstruction_timeline.gif',
        save_all=True,
        append_images=images[1:],
        duration=200,  # Duration for each frame in milliseconds
        loop=0  # 0 means loop forever
    )

    # Now safe to close buffers
    for buf in buffers:
        buf.close()
    print(f"GIF saved to {filename}")



import mfdf
def forward_project(previous_gpsl_image, previous_gpsl_bkg, new_recon, sys_config, recon_config, status):

    # Just repeat the latest background value for the new positions
    last_values = previous_gpsl_bkg.data[-1, :]
    num_to_pad = new_recon.meas.traj.num_nodes - previous_gpsl_bkg.data.shape[0]
    bkg_to_pad = np.tile(last_values, (num_to_pad, 1))
    bkg_padded = np.concatenate([previous_gpsl_bkg.data, bkg_to_pad], axis=0)
    

    min_nll_idx = np.argmin(previous_gpsl_image["nll"])
    optimal_xyz = previous_gpsl_image.xyz[min_nll_idx]
    optimal_weight = previous_gpsl_image["weights"][min_nll_idx]
    # Detector response
    source_energy = 1000
    resp = sys_config.get_response('gpsl')
    resp_grid = resp.calc_grid(
        energy=source_energy,
        rotation=sys_config.get_response_rotation('gpsl'),
        step_deg=1,
        inclusive=True,
    )
    air_density = 1.20479
    air_att_coeff = mfdf.tools.attenuation.compute_lin_att_coeff_air(
        source_energy, air_density
    )
    # Perform forward projection
    fp = mfdf.algorithms.gpsl.gpsl_fp(
        xyz=optimal_xyz,
        weight=optimal_weight,
        pose_xyz=new_recon.meas.traj.get("translations", concat=True).xyz,
        pose_rot=new_recon.meas.traj.get("rotations", concat=True).matrices,
        pose_dt=new_recon.meas.traj.get("dt", concat=True),
        det_response=resp_grid,
        bkg_estimate=bkg_padded,
        det_pos=None,
        tik_dist=sys_config.get_param("det_radius"),
        air_att_coeff=air_att_coeff,
        device=recon_config.get_param("device"),
        device_mem_max=recon_config.get_param("device_mem_max"),
        status=status,
    )
    return fp