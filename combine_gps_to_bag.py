import rosbag
from sensor_msgs.msg import NavSatFix
from rospy.rostime import Time
import pa_mines

# Path to your existing and new ROS bag
existing_bag_path = '/workspace/data/data.bag'
new_bag_path = '/workspace/data/data_combined_w_gps.bag'
gps_topic = '/gps/navsat'
gps_data = pa_mines.read_gps_csvs('/workspace/gps_csvs')

# Cut the correct time slice from the GPS data
bag = rosbag.Bag(existing_bag_path)
start_time = bag.get_start_time()
end_time = bag.get_end_time()
gps_data = gps_data[(gps_data.index >= start_time) & (gps_data.index <= end_time)]
# Create a new ROS bag
with rosbag.Bag(new_bag_path, 'w') as new_bag:
    # Copy all messages from the existing bag to the new bag
    with rosbag.Bag(existing_bag_path, 'r') as existing_bag:
        for topic, msg, t in existing_bag.read_messages():
            new_bag.write(topic, msg, t)

    # Now add the GPS data
    for index, row in gps_data.iterrows():
        gps_msg = NavSatFix()
        gps_msg.header.stamp = Time.from_sec(row.name)
        gps_msg.latitude = row['lat']
        gps_msg.longitude = row['lon']
        gps_msg.altitude = row['alt']

        # Write the GPS message to the new bag
        new_bag.write(gps_topic, gps_msg, gps_msg.header.stamp)
print('GPS data added to the bag.')
