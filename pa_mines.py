import os
import contextily as ctx
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pytz
import h5py


def read_gps_csvs(gps_folder):
    all_gps_csv = os.listdir(gps_folder)
    dfs = [pd.read_csv(os.path.join(gps_folder, csv), sep=";") for csv in all_gps_csv]
    # drop first column
    dfs = [df.drop(df.columns[0], axis=1) for df in dfs]
    # rename columns
    for df in dfs:
        df.columns = ["datetime", "lat", "lon", "asml"]
        df["alt"] = df.asml - df.asml[0]
    # Add a UNIX timestamp column to each dataframe
    for df in dfs:
        df["ts"] = df["datetime"].apply(make_unix)
    merged_df = pd.concat(dfs)
    merged_df.sort_values("ts", inplace=True)
    merged_df.reset_index(inplace=True, drop=True)
    merged_df.set_index("ts", inplace=True)
    # merged_df.drop('ts', axis=1, inplace=True)
    merged_df = merged_df.loc[~merged_df.index.duplicated(keep="first")]
    return merged_df


def make_unix(timestamp_str):
    # Convert the timestamp string to a datetime object
    timestamp = pd.to_datetime(timestamp_str)
    timestamp = timestamp.tz_localize("US/Pacific")
    # Convert the datetime object to a Unix timestamp
    return timestamp.timestamp()


def read_all_h5_files(input_folder, exclude_runs=[]):
    h5_files = []
    for root, dirs, files in os.walk(input_folder):
        for file in files:
            if file.endswith(".h5"):
                h5_files.append(os.path.join(root, file))
    dfs = []
    for h5_file in h5_files:
        print(f"Reading {h5_file}")
        if h5_file not in exclude_runs:
            df = read_h5_folder(h5_file)
            folder_name = os.path.basename(os.path.dirname(h5_file))
            df["run"] = folder_name
            df["run"] = df["run"].astype(str)
            dfs.append(df)
    df = pd.concat(dfs)
    df.sort_values("ts", inplace=True)
    df.reset_index(inplace=True, drop=True)
    return df


def read_h5_folder(input_path):
    if os.path.isdir(input_path):
        h5_file = [f for f in os.listdir(input_path) if f.endswith(".h5")][0]
    elif input_path.endswith(".h5"):
        h5_file = input_path
    else:
        raise ValueError("No h5 file found in the input folder")
    with h5py.File(os.path.join(input_path, h5_file), "r") as f:
        # print("Keys: %s" % f['rad'].keys())
        if 'rad' in f.keys():
            # group_keys = f['interaction_data'].keys()
            # print("Group Keys: %s" % group_keys)
            # imu_z = f["imu_ros"]["angular_velocity.z"][()]
            # imu_x = f["imu_ros"]["angular_velocity.x"][()]
            # imu_y = f["imu_ros"]["angular_velocity.y"][()]
            # imu_t = f["imu_ros"]["header.stamp"][()]
            # count_rate = f["nanomca"]["count_rate"][()]
            # rad_energy = f["rad"]["arrays"]["spectra"][()]
            rad_energy = f["rad"]["arrays"]["energy"][()]
            rad_timestamps = f["rad"]["arrays"]["timestamp_det"][()]
        elif 'energy' in f.keys():
            # print("Keys: %s" % f['energy'].keys())
            rad_energy = f["energy"]["spectra"][()]
            rad_timestamps = f["timestamp_sys"][()]
        else:
            raise ValueError("No rad data found in the h5 file")
    df = pd.DataFrame(
        {
            "energy": rad_energy,
            "ts": rad_timestamps,
        }
    )
    return df
    # return imu_z, imu_x, imu_y, imu_t, count_rate, rad_energy, rad_timestamps

def read_h5_folder_lidar(input_path):
    if os.path.isdir(input_path):
        h5_file = [f for f in os.listdir(input_path) if f.endswith(".h5")][0]
    elif input_path.endswith(".h5"):
        h5_file = input_path
    else:
        raise ValueError("No h5 file found in the input folder")
    with h5py.File(os.path.join(input_path, h5_file), "r") as f:
        rad_energy = f["rad"]["arrays"]["energy"][()]
        rad_timestamps = f["rad"]["arrays"]["timestamp_det"][()]
        
        # Read Velodyne points
        velodyne_points = f["velodyne"]["velodyne_points"][()]
        velodyne_frame_id = f["velodyne"]["header.frame_id"][()]
        velodyne_seq = f["velodyne"]["header.seq"][()]
        velodyne_stamp = f["velodyne"]["header.stamp"][()]
        velodyne_msg_stamp = f["velodyne"]["msg.stamp"][()]

    df = pd.DataFrame(
        {
            "energy": rad_energy,
            "ts": rad_timestamps,
            "velodyne_points": velodyne_points,
            "velodyne_frame_id": velodyne_frame_id,
            "velodyne_seq": velodyne_seq,
            "velodyne_stamp": velodyne_stamp,
            "velodyne_msg_stamp": velodyne_msg_stamp,
        }
    )
    return df


import warnings

def resample_gps(gps_df, times):
    df = gps_df.drop("datetime", axis=1, inplace=False)
    if times.min() < df.index.min() or times.max() > df.index.max():
        warnings.warn("Interpolation is being attempted outside of the range of the original data.")
    resampled_df = df.reindex(df.index.union(times)).interpolate("index").loc[times]
    return resampled_df


def plot_interpolated_path(rad_df, gps_df):
    fig, ax = plt.subplots(figsize=(4, 3), dpi=150)
    ax.plot(rad_df["lon"], rad_df["lat"], color="blue")
    ax.scatter(gps_df["lon"], gps_df["lat"], color="red", s=5)
    ax.set_xlabel("Longitude")
    ax.set_ylabel("Latitude")
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_aspect("equal")


def plot_counts(rad_df, gps_df, time_step=1, time_window=1, normalize=True, zoom_out=0, title=None, ax=None):
    time_bins = np.arange(rad_df.ts.min() - time_window/2, rad_df.ts.max() + time_window/2 + time_step, time_step)

    time_bins = np.arange(rad_df.ts.min(), rad_df.ts.max() + time_step, time_step)
    time_bin_centers = time_bins[:-1] + np.diff(time_bins) / 2
    counts, _ = np.histogram(rad_df.ts, bins=time_bins)
    gps_df_resampled = resample_gps(gps_df, time_bin_centers)
    if ax is None:
        fig, ax = plt.subplots(figsize=(4, 3), dpi=150)
    else:
        fig = ax.get_figure()
    if normalize:
        normalized_counts = (gps_df_resampled["alt"] ** 2) / counts
    else:
        normalized_counts = counts / time_step

    from matplotlib.colors import ListedColormap
    # Custom colormap that approximates MATLAB's 'parula'
    parula_data = [[0.2081, 0.1663, 0.5292], [0.2116, 0.1898, 0.5777], [0.2123, 0.2138, 0.6270],
                [0.2081, 0.2386, 0.6771], [0.1959, 0.2645, 0.7279], [0.1707, 0.2919, 0.7792],
                [0.1253, 0.3242, 0.8303], [0.0591, 0.3598, 0.8683], [0.0117, 0.3875, 0.8820],
                [0.0060, 0.4086, 0.8828], [0.0165, 0.4266, 0.8786], [0.0329, 0.4430, 0.8720],
                [0.0498, 0.4586, 0.8641], [0.0629, 0.4737, 0.8554], [0.0723, 0.4887, 0.8467],
                [0.0779, 0.5040, 0.8384], [0.0793, 0.5200, 0.8312], [0.0749, 0.5375, 0.8263],
                [0.0641, 0.5570, 0.8240], [0.0488, 0.5772, 0.8228], [0.0343, 0.5966, 0.8199],
                [0.0265, 0.6137, 0.8135], [0.0239, 0.6287, 0.8038], [0.0231, 0.6418, 0.7913],
                [0.0228, 0.6535, 0.7768], [0.0267, 0.6642, 0.7607], [0.0384, 0.6743, 0.7436],
                [0.0590, 0.6838, 0.7254], [0.0843, 0.6928, 0.7062], [0.1133, 0.7015, 0.6859],
                [0.1453, 0.7098, 0.6646], [0.1801, 0.7177, 0.6424], [0.2178, 0.7250, 0.6193],
                [0.2586, 0.7317, 0.5954], [0.3022, 0.7376, 0.5712], [0.3482, 0.7424, 0.5473],
                [0.3953, 0.7459, 0.5244], [0.4420, 0.7481, 0.5033], [0.4871, 0.7491, 0.4840],
                [0.5300, 0.7491, 0.4661], [0.5709, 0.7485, 0.4494], [0.6099, 0.7473, 0.4337],
                [0.6473, 0.7456, 0.4188], [0.6834, 0.7435, 0.4044], [0.7184, 0.7411, 0.3905],
                [0.7525, 0.7384, 0.3768], [0.7858, 0.7356, 0.3633], [0.8185, 0.7327, 0.3498],
                [0.8507, 0.7299, 0.3360], [0.8824, 0.7274, 0.3217], [0.9139, 0.7258, 0.3063],
                [0.9450, 0.7261, 0.2886], [0.9739, 0.7314, 0.2666], [0.9938, 0.7455, 0.2403],
                [0.9990, 0.7653, 0.2164], [0.9955, 0.7861, 0.1967], [0.9880, 0.8066, 0.1794],
                [0.9789, 0.8271, 0.1633], [0.9697, 0.8481, 0.1475], [0.9626, 0.8705, 0.1309],
                [0.9589, 0.8949, 0.1132], [0.9598, 0.9218, 0.0948], [0.9661, 0.9514, 0.0755]]
    parula = ListedColormap(parula_data, name='parula')
    from matplotlib.colors import Normalize
    from matplotlib.collections import LineCollection
    line = False
    if line:
        # Create a list of points
        points = np.array([gps_df_resampled["lon"], gps_df_resampled["lat"]]).T.reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)

        # Create a continuous norm to map from data points to colors
        norm = Normalize(vmin=250, vmax=280)
        lc = LineCollection(segments, cmap=parula, norm=norm)
        lc.set_array(normalized_counts)
        lc.set_linewidth(1.5)

        im = ax.add_collection(lc)
    else:
        im = ax.scatter(
            gps_df_resampled["lon"],
            gps_df_resampled["lat"],
            c=normalized_counts,
            # cmap=parula,
            cmap="viridis",
            s=0.5,
            # vmin=250,
            # vmax=300,
            # vmax=0.04
        )
    # im = ax.tricontourf(
    #     gps_df_resampled["lon"],
    #     gps_df_resampled["lat"],
    #     normalized_counts,
    #     cmap="viridis",
    #     levels=100,
    #     vmax=280,
    #     vmin=250,
    # )
    if title:
        ax.set_title(title)
    label = r"Altitude$^2$ / counts" if normalize else r"Counts [s$^{-1}$]"
    fig.colorbar(im, ax=ax, label=label)
    ax.set_xlabel("Longitude")
    ax.set_ylabel("Latitude")
    ax.set_xticks([])
    ax.set_yticks([])
    # ax.set_aspect("equal")
    if zoom_out:
        ax.set_xlim(
            gps_df_resampled["lon"].min() - zoom_out, gps_df_resampled["lon"].max() + zoom_out
        )
        ax.set_ylim(
            gps_df_resampled["lat"].min() - zoom_out, gps_df_resampled["lat"].max() + zoom_out
        )
    ctx.add_basemap(
        ax=ax,
        crs="EPSG:4326",
        source=ctx.providers.Esri.WorldImagery,
        # source=ctx.providers.CartoDB.PositronNoLabels,
        attribution=False,
    )


def plot_waterfall(rad_df, num_bins=100):
    import matplotlib.colors as colors

    energy_bins = np.linspace(rad_df.energy.min(), rad_df.energy.max(), num=num_bins)
    time_bins = np.linspace(rad_df.ts.min(), rad_df.ts.max(), num=num_bins)
    counts, _, _ = np.histogram2d(
        rad_df.energy, rad_df.ts, bins=[energy_bins, time_bins]
    )
    plt.figure(figsize=(4, 3), dpi=150)
    plt.imshow(
        counts.T,
        aspect="auto",
        origin="lower",
        extent=[
            energy_bins.min(),
            energy_bins.max(),
            time_bins.min() - time_bins.min(),
            energy_bins.max() - energy_bins.min(),
        ],
        norm=colors.LogNorm(),
    )
    plt.colorbar(label="Counts")
    plt.xlabel("Energy")
    plt.ylabel("Time from start (s)")
