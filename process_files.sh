#!/bin/bash

# Activate the conda environment
source activate rot
echo "Conda environment activated"

# Find and process .bag files
find . -type f -name '*.bag' -print0 | while IFS= read -r -d '' file; do
    echo "Processing $file"
    cd "$(dirname "$file")" || exit

    # Initialize rot
    rot init "$(basename "$file")"

    # Convert .bag file to h5 with additional messages
    rot h5 --additional-messages all *.bag
    cd - || exit
done
